module.exports = {
    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = "APUE~";
                return args;
            });

        config.module.rule('yaml')
            .test(/\.ya?ml$/)
            .use('json-loader')
                .loader('json-loader')
            .end()
            .oneOf('stream')
                .resourceQuery(/stream/)
                .use('yaml-loader')
                    .loader('yaml-loader')
                    .options({ asStream: true })
                .end()
            .end()
            .oneOf('normal')
                .use('yaml-loader')
                    .loader('yaml-loader')
                .end()
            .end()
            // .oneOf([
            //     {
            //         resourceQuery: /stream/,
            //         options: { asStream: true },
            //         use: 'yaml-loader',
            //     },
            //     { use: 'yaml-loader' },
            // ])
            // .use('yaml-loader')
            //     .loader('yaml-loader')
            //     .option()
            // .end()
        ;

        config.module
            .rule('images')
            .use('url-loader')
                .loader('url-loader')
                // .tap((options) => {
                    // options.fallback.options.name = 'img/[name].[ext]';
                    // return options;
                // })
            .end()
            // .use('image-loader')
            //     .loader('image-loader')
            // .end()
            .use('image-maxsize-webpack-loader')
                .loader('image-maxsize-webpack-loader')
                .tap(() => ({
                    useImageMagick: true,
                }))
            .end()
        ;
    },
    productionSourceMap: false,
};
