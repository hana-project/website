import Vue from 'vue'
import VueRouter from 'vue-router';
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import App from './App.vue'
import router from './router';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.component('font-awesome-icon', FontAwesomeIcon);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
});

const store = new Vuex.Store({
  state: {
    darkmode: false,
  },
  mutations: {
    setDarkmode(state, value) {
      state.darkmode = value;
    },
  },
  plugins: [
      vuexLocal.plugin,
  ],
})

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
